<?php

function findRoute(string $path): array
{
    $routes = json_decode(file_get_contents(__DIR__.'/routes.json'),1);

    if (!$routes)
        die("No routing files found.");

    foreach ($routes as $r) {
        if ($r["path"] === $path) {
            $route = $r;
            break;
        }
    }

    return $route ?? $routes["home"];
}
