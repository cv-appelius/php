<?php

use App\Engine\Response;

require_once dirname(__DIR__).'/vendor/autoload.php';
include_once dirname(__DIR__).'/config/routing.php';

$uri = $_SERVER["REQUEST_URI"];

$route      = findRoute($uri);
$controller = explode("::", "\\".$route["controller"]);

/** @var Response $resp */
$resp = call_user_func([(new $controller[0]()), $controller[1]]);

return $resp->view();
