<?php

namespace App\Engine;

use App\Consts\General;
use App\Engine\PlatesFunctions;
use League\Plates\Engine;

final class Response implements General
{
    const TEMPLATES_DIR = "/templates/";
    const BUILD_URI     = '%s://%s';

    /** @var array $vars */
    protected $vars;

    /** @var string $vars */
    protected $file;

    public function __construct(string $file, array $vars = [])
    {
        $this->setFile($file);
        $this->setVars($vars);
    }

    public function view(): void
    {
        $engine = new Engine(dirname(__DIR__, 2).static::TEMPLATES_DIR);

        PlatesFunctions::addFunctionsToPlates($engine);

        echo $engine->render($this->getFile(), $this->getVars());

        die();
    }

    private function setVars(array $vars): self
    {
        $this->vars = $vars;

        return $this;
    }

    private function getVars(): array
    {
        return $this->vars;
    }

    private function setFile(string $file): self
    {
        $this->file = ltrim(str_replace(static::DOT_PHP, '', $file), static::SLASH);

        return $this;
    }

    private function getFile(): string
    {
        return $this->file;
    }

    static function getBasePath(): string
    {
        $requestScheme = $_SERVER[static::REQUEST_SCHEME] ?? static::HTTP;
        $host          = $_SERVER[static::HTTP_HOST] ?? '';

        return sprintf(static::BUILD_URI, $requestScheme, $host);
    }
}
