<?php

namespace App\Engine;

use App\Consts\General;
use League\Plates\Engine;

final class PlatesFunctions implements General
{
    const ASSET_PATH = '/assets/%s';

    const FUNCTIONS = [
        self::ASSET_URL,
    ];

    static function addFunctionsToPlates(Engine $engine): void
    {
        foreach (static::FUNCTIONS as $func)
            forward_static_call([static::class, $func], $engine);
    }

    static function assetUrl(Engine $engine): void
    {
        $engine->registerFunction(__FUNCTION__, function(string $string): string {
            $string = ltrim($string, static::SLASH);

            return Response::getBasePath().sprintf(static::ASSET_PATH, $string);
        });
    }
}
