<?php

namespace App\Utils;

use App\Consts\General;

final class Api implements General
{
    const API_URL     = 'https://api.appelius-theo.fr';
    const BASE_CONFIG = [
        CURLOPT_TIMEOUT        => 5,
        CURLOPT_CONNECTTIMEOUT => 5,
        CURLOPT_RETURNTRANSFER => true,
    ];

    static function get(string $route): array
    {
        return static::base($route, static::BASE_CONFIG);
    }

    static function base(string $route, array $configs = []): array
    {
        $url = static::API_URL.$route;
        $ch  = curl_init($url);

        foreach ($configs as $key => $value)
            curl_setopt($ch, $key, $value);

        $data = curl_exec($ch);
        curl_close($ch);

        if (!$data)
            throw new \Exception(sprintf(static::ERROR_CALL, $url));

        $json = json_decode($data, true);

        return $json[static::DATA] ?? $json;
    }
}
