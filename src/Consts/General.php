<?php

namespace App\Consts;

interface General
{
    const ASSET_URL      = "assetUrl";
    const BIRTHDAY       = "birthday";
    const DATA           = "data";
    const DATE           = "date";
    const DATE_FR_FORMAT = "d/m/Y";
    const DOT_PHP        = ".php";
    const EDUCATION      = "education";
    const ERROR_CALL     = 'l\'appel à l\'url suivante : %s a échoué.';
    const HTTP_HOST      = "HTTP_HOST";
    const HTTP           = "http";
    const PROFILE        = "profile";
    const REQUEST_SCHEME = "REQUEST_SCHEME";
    const SKILLS         = "skills";
    const SLASH          = "/";
    const WORKING        = "working";
}
