<?php

namespace App\Controller;

use App\Consts\General;
use App\Engine\Response;
use App\Utils\Api;

abstract class BaseController implements General
{
    public function render(string $templateFile, array $vars = []): Response
    {
        return new Response($templateFile, $vars);
    }

    public function apiGet(string $route): array
    {
        return Api::get($route);
    }
}
