<?php

namespace App\Controller;

use App\Engine\Response;

final class DefaultController extends BaseController
{
    const ROUTE_PROFILE   = "/profile/";
    const ROUTE_SKILLS    = "/skills/";
    const ROUTE_EDUCATION = "/history/education";
    const ROUTE_WORKING   = "/history/working";

    public function index(): Response
    {   
        $profile = $this->apiGet(static::ROUTE_PROFILE);
        if (isset($profile[0])) {
            $profile[0][static::BIRTHDAY] = (new \DateTime($profile[0][static::BIRTHDAY][static::DATE]))->format(static::DATE_FR_FORMAT);
            $profile                      = $profile[0];
        }

        return $this->render('default/index.php', [
            static::PROFILE   => $profile,
            static::SKILLS    => $this->apiGet(static::ROUTE_SKILLS),
            static::EDUCATION => $this->apiGet(static::ROUTE_EDUCATION),
            static::WORKING   => $this->apiGet(static::ROUTE_WORKING),
        ]);
    }
}
