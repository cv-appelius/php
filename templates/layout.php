<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?=$this->section('meta');?>

    <title><?=$this->e($title);?></title>

	<?=$this->section('css');?>
</head>
<body id="app">
    <?=$this->section('html_content');?>
</body>
</html>
