<?php $this->layout('layout', ['title' => 'CV Appelius Théo - PHP MVC maison']); ?>

<?php $this->start('meta'); ?>
<meta name="robots" content="noindex, nofollow" />
<meta name="googlebot" content="noindex, nofollow" />
<meta property="og:title" content="Appelius Théo">
<meta property="og:description" content="Appelius Théo développeur indépendant fullstack Français situé en région Bordelaise spécialisé sur les technologies suivantes : PHP Symfony et Vuejs.">
<meta property="og:url" content="https://php.appelius-theo.fr">
<meta name="twitter:card" content="summary_large_image">
<?php $this->stop(); ?>

<?php $this->start('css'); ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
	<link rel="stylesheet" href="<?= $this->assetUrl('css/normalize.css'); ?>" type="text/css">
	<link rel="stylesheet" href="<?= $this->assetUrl('fontawesome/css/all.min.css'); ?>" type="text/css">
	<link rel="stylesheet" href="<?= $this->assetUrl('css/main.css'); ?>" type="text/css" media="screen">
	<link rel="stylesheet" href="<?= $this->assetUrl('css/profile.css'); ?>" type="text/css" media="screen">
	<link rel="stylesheet" href="<?= $this->assetUrl('css/navbar.css'); ?>" type="text/css" media="screen">
	<link rel="stylesheet" href="<?= $this->assetUrl('css/skills.css'); ?>" type="text/css" media="screen">
	<link rel="stylesheet" href="<?= $this->assetUrl('css/history.css'); ?>" type="text/css" media="screen">
	<link rel="stylesheet" href="<?= $this->assetUrl('css/print.css'); ?>" type="text/css" media="print">
<?php $this->stop(); ?>

<?php $this->start('html_content'); ?>
<div class="card headband-container">
    <div class="d-flex d-row headband w-100">
        <a href="https://vuejs.appelius-theo.fr" target="_blank" title="Voir aussi en Vuejs">
            <i class="fab fa-vuejs" aria-hidden="true"></i>
        </a>

        <a href="https://appelius-theo.fr" target="_blank" title="Voir aussi en HTML & JavaScript natif">
            <i class="fab fa-html5" aria-hidden="true"></i>
        </a>
    </div>
</div>

<div id="wrapper" class="d-flex f-row-m f-column-s">
    <div class="profile__container card d-flex f-column">
        <div class="d-flex f-column profile__header">
            <span class="profile__name"><?= $profile["name"]; ?></span>
            <span class="profile__job"><?= $profile["jobTitle"]; ?></span>
        </div>

        <div class="profile__figure">
            <img src="<?= $this->assetUrl('img/index.jpg'); ?>" alt="Photo de profil">
        </div>

        <ul class="profile__information">
            <li>
                <p>
                    <span>Nom&nbsp;:</span> <?= $profile["name"]; ?>
                </p>
            </li>
            <li>
                <p>
                    <span>Date de naissance&nbsp;:</span> <?= $profile["birthday"]; ?>
                </p>
            </li>
            <li>
                <p>
                    <span>Email&nbsp;:</span> <?= $profile["email"]; ?>
                </p>
            </li>
        </ul>

        <div class="profile__social-network-container d-flex f-row">
            <a class="profile__social-network" target="_blank" href="https://gitlab.com/AppeliusTheo">
                <i class="fab fa-gitlab" aria-hidden="true"></i>
            </a>
            <a class="profile__social-network" target="_blank" href="https://www.linkedin.com/in/appelius">
                <i class="fab fa-linkedin" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div class="tab__container d-flex f-column">
        <header>
            <nav class="card navbar d-flex f-row">
                <ul class="navbar__tabs d-flex f-row">
                    <li>
                        <a class="navbar__a d-flex" href="#about-me">
                            <i class="fas fa-home" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#skills">Compétences</a>
                    </li>
                    <li>
                        <a href="#working-">Expériences</a>
                    </li>
                    <li>
                        <a href="#education-history">Formation</a>
                    </li>
                </ul>

                <div class="navbar__social-networks d-flex f-row">
                    <a class="navbar__network-a" target="_blank" href="https://gitlab.com/cv-appelius/php" title="Cliquer pour voir le code">
                        <i class="fas fa-code" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </header>

        <main role="main" class="card main-content">
            <section id="about-me">
                <h2>à propos de moi</h2>
                <p class="top-30">
                    <?= $profile["description"]; ?>
                </p>
            </section>

            <section id="skills" class="sub-card">
                <h2>compétences</h2>
                <div class="top-30 d-flex f-row-m f-column-s skills__container">
                    <?php foreach ($skills as $skill) : ?>
                        <div class="skill__unit" title="<?= $skill['percent'].'%'; ?>">
                            <h3><?= $skill["name"]; ?></h3>
                            <div class="skill__progress w-100">
                                <div class="<?= 'skill__percentage w-'.$skill['percent']; ?>"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </section>

            <section id="working-history">
                <h2>expériences</h2>
                <div class="top-30 d-flex f-row-m f-column-s history__container">
                    <?php foreach ($working as $w) : ?>
                        <div class="history__unit">
                            <div class="history__header d-flex f-column">
                                <span class="history__title"><?= $w["jobTitle"]; ?></span>
                                <span class="history__date"><?= $w["from"]." - ".$w["to"]; ?></span>
                            </div>
                            <p><?= $w["description"]; ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </section>

            <section id="education-history" class="sub-card">
                <h2>formation</h2>
                <div class="top-30 d-flex f-row-m f-column-s history__container">
                    <?php foreach ($education as $ed) : ?>
                        <div class="history__unit">
                            <div class="history__header d-flex f-column">
                                <span class="history__title"><?= $ed["jobTitle"]; ?></span>
                                <span class="history__date"><?= $ed["from"]." - ".$ed["to"]; ?></span>
                            </div>
                            <p><?= $ed["description"]; ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </section>
        </main>
    </div>
</div>
<?php $this->stop(); ?>
